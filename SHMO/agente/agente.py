#!/usr/bin/python
# -*- coding: utf-8; mode: python -*-

from flask import Flask, request, redirect, url_for, g, session, flash, render_template,json,jsonify

import json
import urllib3
http = urllib3.PoolManager(port = '5000')
import requests
import pandas
import time

app = Flask(__name__)

app.secret_key ="aa"

def getStop():
	f = open("CONFIG")
	dato = f.read(1)
	f.close()
	return dato

def setStop(status):
	f=open("CONFIG","w")
	f.write(status)
	f.close()


@app.route('/', methods=['GET'])
def index():
	return render_template('index.html')

@app.route('/escanear', methods=['POST'])
def escaner():
	token=request.form.get("agente")
	indata=int(request.form.get('comp_select'))
	############ INICIO ###################
	setStop("0")
	r = requests.post(url='http://127.0.0.1:5000/enviar/inicio/'+str(token))
	id_envio=int(r.text)
	contador=0
	#######################################

	############# ENVIO ###################

	if(indata==11):
		ruta="modelo/datos/1.csv"
	else:
		ruta="modelo/datos/0.csv"

	print indata
	df = pandas.read_csv(ruta)
	jsonFile = df.to_json('file.json', orient='records')
	with open('file.json') as file:
		data = json.load(file)
	for i in data:
		contador=contador+1
		encoded_data = json.dumps(i).encode('utf-8')
		r = requests.post(url='http://127.0.0.1:5000/enviar/'+str(id_envio), data=encoded_data, headers={'Content-Type': 'application/json'})
		print r.text

		if(getStop()=="1"):
			break
    		time.sleep(1)
	   	  
	r = requests.post(url='http://127.0.0.1:5000/enviar/stop/'+str(id_envio))
	########################################
	return redirect("/")


@app.route('/stop', methods=['POST'])
def stopEnvio():
	setStop("1")
	return redirect("/")


if __name__ == '__main__':
    print("Hola")
    app.run(debug=True,port=5001)
