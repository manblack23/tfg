import io

from setuptools import find_packages, setup

with io.open('README.rst', 'rt', encoding='utf8') as f:
    readme = f.read()

setup(
    name='Sherlock and Moriarty',
    version='1.0.0',
    url='http://sergioalises.com',
    license='BSD',
    maintainer='Sergio Alises',
    maintainer_email='sergio.alises@alu.uclm.es',
    description='A WebApp for audit Android devices',
    long_description=readme,
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        'flask','flask_oauthlib','requests','datetime','pickle-mixin','pandas','sklearn','pygal'
    ],
    extras_require={
        'test': [
            'pytest',
            'coverage',
        ],
    },
)
