Sherlock and Moriarty
======

A WebApp_ for audit Android devices.

.. _WebApp: https://sergioalises.com


Crea un entorno virtualenv y activalo::

    $ python2 -m virtualenv venv
    $ . venv/bin/activate

Realiza el set-up::

    $ make init


Para ejecutar la aplicación una vez hecho el Set-Up::

    $ make run

Dirígete a http://127.0.0.1:5000 y http://127.0.0.1:5001 en un navegador.



