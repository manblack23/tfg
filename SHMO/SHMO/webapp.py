# -*- coding: utf-8 -*-

from flask import Flask, request, redirect, url_for, g, session, flash, render_template,json,jsonify
from flask import (
    Blueprint, flash, g,abort,redirect, render_template, request, session, url_for
)
import pygal
import os.path 
import time
import datetime
import requests
from SHMO.db import get_db
from ConfigParser import ConfigParser
from SHMO.auth import login_required
from sklearn.metrics import classification_report

####
import pandas as pd
import pickle
####

bp = Blueprint('webapp', __name__, url_prefix='/')

def obtenerDatosTwitter():
	name = session['name']

	if 'profile_image_url_https' not in session:
		nombreImagen="Usuario.jpg"
	else:
		linkImagen = session['profile_image_url_https']
		linkImagen_new = linkImagen.split("_normal")[0]+"_400x400.jpg"
		nombreImagen = linkImagen_new.split("/")
		nombreImagen = nombreImagen[len(nombreImagen)-1]
		pathImg = "static/"+nombreImagen
		ruta = "SHMO/static/images/"+nombreImagen
		try:
	    		r = requests.get(linkImagen_new)
			with open(ruta, 'wb') as f:  
					f.write(r.content)
	    	except:
	        	print("Error al descargar la imagen")

	return name,nombreImagen

@bp.route('/')
@login_required
def inicio():
	return redirect(url_for('webapp.index'))

@bp.route('/usuarios')
@login_required
def device():
	name, nombreImagen = obtenerDatosTwitter()
	db = get_db()
	users = db.execute(
		'SELECT id,nombre, apellidos, token'
		' FROM user'
    	).fetchall()

    	
	return render_template("webapp/usuarios.html", nombre=name,imagen_user="images/"+nombreImagen,users=users)

@bp.route('/modelo')
@login_required
def modelo():
	name, nombreImagen = obtenerDatosTwitter()
	return render_template("webapp/modelo.html", nombre=name,imagen_user="images/"+nombreImagen)

@bp.route('/ayuda')
@login_required
def ayuda():
	name, nombreImagen = obtenerDatosTwitter()
	return render_template("webapp/ayuda.html", nombre=name,imagen_user="images/"+nombreImagen)

def get_user(id):
    """
    :return: el usuario con su informacion
    :raise 404: si el usuario no existe then 404 not found

    """
    db = get_db()
    user = db.execute(
        'SELECT id,nombre, apellidos, token'
        ' FROM user '
        ' WHERE user.id = ?',
        (id,)
    ).fetchone()

    if user is None:
        abort(404, "User id {0} doesn't exist.".format(id))

    return user

def get_userToken(token):
    """
    :return: el usuario con su informacion
    :raise 404: si el usuario no existe then 404 not found

    """
    db = get_db()
    user = db.execute(
        'SELECT id,nombre, apellidos'
        ' FROM user '
        ' WHERE user.token = ?',
        (token,)
    ).fetchone()

    if user is None:
        abort(404, "token {0} doesn't exist.".format(token))

    return user

@bp.route('/addUser', methods=['POST'])
@login_required
def addUser():
	nombre=request.form.get("usrnm")
	apellidos=request.form.get("surname")
	token=request.form.get("token")
	db = get_db()

	db.execute(
                'INSERT INTO user (Nombre, Apellidos, token)'
                ' VALUES (?, ?, ?)',
                (nombre, apellidos, token)
            )
	db.commit()
	print nombre, apellidos,token
	return redirect('usuarios')


@bp.route('/user/<int:id_us>')
@login_required
def user(id_us):
	name, nombreImagen= obtenerDatosTwitter()
	id_user =id_us
	id_us = str(id_us).decode('utf-8')
	nomApe= str(get_user(id_us)[1])+" "+ str(get_user(id_us)[2])
	db = get_db()
	estadisticas= db.execute(
        	'SELECT id_envio,fechaInicio,fechaFin,nreg'
        	' FROM envios '
        	' WHERE envios.id_user = ?',
        	(id_user,)
    	).fetchall()
	if estadisticas:
		stopID=estadisticas[len(estadisticas)-1]['id_envio']
    	return render_template("webapp/usuarioAv.html",user=id_us,nombre=name,imagen_user="images/"+nombreImagen,nomApe=nomApe,estadisticas=estadisticas,id_us=id_us)

@bp.route('/delete/<int:id_del>', methods=["GET"])
@login_required
def delete(id_del):
	id = str(id_del).decode("utf-8")
	get_user(id)
	db = get_db()
	db.execute('DELETE FROM user WHERE id = ?', (id,))
	db.commit()
	return redirect('usuarios')

@bp.route('/shmo')
@login_required
def index():
	name, nombreImagen = obtenerDatosTwitter()
	nmal=41
	graph = pygal.Bar()
	graph.title = 'Malware detectado en el ultimo mes'
	x=range(1,31)
	
	graph.x_labels = list(x)
	graph.add('Amenazas', [4,1,2,0,1,1,0,3,1,0,0,0,0,1,2,5,2,0,2,1,1,1,4,0,1,0,0,3,2,3,0])
	graph_data = graph.render_data_uri()

	valoresReturn=[]
	valoresReturn.append(ndispositivos())
	valoresReturn.append(nmal)
	valoresReturn.append(nregistros())
	return render_template("webapp/index.html", nombre=name,imagen_user="images/"+nombreImagen,chart = graph_data,valoresReturn=valoresReturn)

@bp.route('/enviar/<int:id_envio>', methods=['POST','GET'])
def add_message2(id_envio):
	content = request.json
	db = get_db()
	db.execute(
                'INSERT INTO datos (id_envio,"4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26")'
                ' VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
                (id_envio,content['4'],content['5'],content['6'],content['7'],content['8'],content['9'],content['10'],content['11'],content['12'],content['13'],content['14'],content['15'],content['16'],content['17'],content['18'],content['19'],content['20'],content['21'],content['22'],content['23'],content['24'],content['25'],content['26'])
            )
	db.commit()


	return "Registro recibido correctamente"
	
@bp.route('/enviar/inicio/<int:token>',methods=['POST'])
def init(token):
	db = get_db()
	user =get_userToken(str(token))
	user_id =user['id']
    	idenv = db.execute(
        	'SELECT id_envio'
        	' FROM datos '
        	' order by id_envio desc limit 1'
    	).fetchone()
	if idenv is None:
		idenv=1
	else:
		idenv=int(idenv[0])+1
	print(idenv)
	
	
	db.execute(
                'INSERT INTO envios (id_envio, id_user,fechainicio)'
                ' VALUES (?,?,?)',
                (idenv,user_id,datetime.datetime.now())
            )
	db.commit()
	
	
	return str(idenv)
 

@bp.route('/modelo/<int:id_us>',methods=['GET'])
@login_required
def modeloUser(id_us):
	name, nombreImagen = obtenerDatosTwitter()
	id_us=str(id_us)
	user = get_user(id_us)
	nombreUser = user[1]+" "+user[2]
	numNeg=0
	numPos=1
	listaValores = pred(id_us)
	numEscaneados = len(listaValores)
	numNeg = listaValores.tolist().count(1)
	numPos = listaValores.tolist().count(0)
	negbuen=100*numNeg/len(listaValores)
	print(negbuen)
	textoV= "El resultado del analisis presenta pocos registros maliciosos como para considerarlo infectado. El dispositivo esta libre de malware."
	textoM= "El dispositivo esta infectado, por favor, analice las aplicaciones instaladas."
	return render_template('webapp/modeloUser.html',textoV=textoV,textoM=textoM,nombre=name,imagen_user="images/"+nombreImagen,nombreUser=nombreUser,numNeg=numNeg,numPos=numPos,numEscaneados=numEscaneados,negbuen=negbuen)


@bp.route('/enviar/stop/<int:id_us>',methods=['GET'])
def stopEnvioServer(id_us):
	id_us = str(id_us)
	r = requests.post(url='http://127.0.0.1:5001/stop')

	return redirect('user/'+id_us)

@bp.route('/enviar/stop/<int:id_envio>',methods=['POST'])
def stopEnvio(id_envio):
	
	db = get_db()
	nreg = db.execute(
        	'SELECT count(*)'
        	' FROM datos '
        	' where datos.id_envio= ?',
        	(id_envio,)
    	).fetchone()
	nreg= nreg[0]
	db.execute(
		'UPDATE envios SET fechafin = ?, nreg = ? WHERE id_envio = ?',
        (datetime.datetime.now(), nreg, id_envio))
	db.commit()

	return "pepe"

def queris(id_user):
	id_user = int(id_user)
	db = get_db()
	envio = db.execute(
        	'SELECT id_envio'
        	' FROM envios '
        	' where envios.id_user= ?'
		'order by id_envio desc limit 1',
        	(id_user,)
    	).fetchone()
	envio = int(envio[0])

	datosa = """SELECT * FROM datos WHERE datos.id_envio ={} """.format(envio)
	df = pd.read_sql_query(datosa,db)

	return df

def pred(id_user):
	model = pickle.load(open('modelo/model2.pkl','rb'))
	df = queris(id_user)
	df = df.replace("null",0)
	pred = df[df.columns[3:24]]
	
	
	prediction = model.predict(pred)
	print(prediction)
	return prediction

def ndispositivos():
	db = get_db()
	nusertotal = db.execute(
        	'SELECT count(*)'
        	' FROM user '
    	).fetchone()
	return int(nusertotal[0])


def nregistros():
	db = get_db()
	nregtotal = db.execute(
        	'SELECT count(*)'
        	' FROM datos '
    	).fetchone()

	return int(nregtotal[0])



