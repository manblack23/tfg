import functools

from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for
)
from flask_oauthlib.client import OAuth

from werkzeug.security import check_password_hash, generate_password_hash

from SHMO.db import get_db

bp = Blueprint('auth', __name__, url_prefix='/auth')


oauth = OAuth()
bp.secret_key ="rZywJFy4kEVVFzfJYP3QVyHSK1EsE2UYtTQ50o4fWOd2q"
twitter = oauth.remote_app('twitter',
    base_url='https://api.twitter.com/1.1/',
    request_token_url='https://api.twitter.com/oauth/request_token',
    access_token_url='https://api.twitter.com/oauth/access_token',
    authorize_url='https://api.twitter.com/oauth/authenticate',
    consumer_key='oCwdimMOnUm429Nn3GA6KQBRn',
    consumer_secret='e8VMZFzsqHnjjQ36I1dodp6dpT9W5UPUU52XhE8yCYykSnDmpi'
)


def login_required(view):
    """Redirecciona usuarios anonimos a la pagina de login."""
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if g.user is None:
            return redirect('auth/login')

        return view(**kwargs)

    return wrapped_view
    
# Limpiar sesion anterior e incluir la nueva sesion
@bp.before_app_request
def before_request():
    if 'twitter_oauth' in session:
        g.user = session['twitter_oauth']
    elif('user_id' in session):
        user_id = session['user_id']
        g.user = get_db().execute(
            'SELECT * FROM userAuditor WHERE id = ?', (user_id,)
        ).fetchone()
    else:
        g.user = None

# Obtener token para esta sesion
@twitter.tokengetter
def get_twitter_token(token=None):
    if 'twitter_oauth' in session:
        resp = session['twitter_oauth']
        return resp['oauth_token'], resp['oauth_token_secret']

# Callback
@bp.route('/oauthorized')
def oauthorized():
    resp = twitter.authorized_response()
    if resp is None:
        flash('You denied the request to sign in.')
    else:
        session['twitter_oauth'] = resp
        idse = session['twitter_oauth']['user_id']
        resp = twitter.get("users/show.json?user_id={0}".format(idse))
        session['name'] = resp.data['name']
        session['profile_image_url_https'] = resp.data['profile_image_url_https']
    return redirect(url_for('webapp.index'))

@bp.route('/register', methods=('GET', 'POST'))
def register():
    """Registra un nuevo usuario.
    Valida que el username no se esta usando. Se hashea la
    password por seguridad.
    """
    if request.method == 'POST':
        username = request.form.get('name-picked')
        password = request.form.get('pass-picked')
        db = get_db()
        error = None

        if not username:
            error = 'Introduzca el nombre de usuario.'
        elif not password:
            error = 'Introduzca password'
        elif db.execute(
            'SELECT id FROM userAuditor WHERE username = ?', (username,)
        ).fetchone() is not None:
            error = 'El usuario {0} ya estaba regitrado.'.format(username)

        if error is None:
            # the name is available, store it in the database and go to
            # the login page
            db.execute(
                'INSERT INTO userAuditor (username, password) VALUES (?, ?)',
                (username, generate_password_hash(password))
            )
            db.commit()
            return redirect('auth/login')
        print("ERROR#######",error)
        flash(error)

    return render_template('auth/auth.html',feedbackReg=error)



@bp.route('/login')
def loguin():
    return render_template("auth/auth.html")

# Get auth token (request)
@bp.route('/loginTwitter')
def loginTwitter():
    callback_url=url_for('auth.oauthorized', next=request.args.get('next'))
    return twitter.authorize(callback=callback_url or request.referrer or None)

@bp.route('/loginUser', methods=('GET', 'POST'))
def loginUser():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        db = get_db()
        error = None
        user = db.execute(
            'SELECT * FROM userAuditor WHERE username = ?', (username,)
        ).fetchone()

        if user is None:
            error = 'Credenciales incorrectas.'
        elif not check_password_hash(user['password'], password):
            error = 'Credenciales incorrectas.'

        if error is None:
            session.clear()
            session['user_id'] = user['id']
            session['name'] = user['username'] 
            return redirect(url_for('webapp.index'))
        print(error)
        flash(error)

    return render_template('auth/auth.html',feedback=error)


# Eliminar sesion
@bp.route('/logout')
def logout():
    session.pop('twitter_oauth', None)
    session.pop('user_id',None)
    return redirect('auth/login')
