#!/bin/bash

pip install -e .
export FLASK_APP=SHMO
export FLASK_ENV=development
flask init-db
flask run
