#!/usr/bin/env python
# coding: utf-8

import numpy as np
from scipy.stats import randint as sp_randint
from sklearn.model_selection import RandomizedSearchCV
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
import pickle
import pandas as pd
import matplotlib.pyplot as plt

# REGISTROS DEL AGENTE

agenteFrame = pd.read_csv('../OutFiles/modelo/agenteFrame.csv')
agenteFrame = agenteFrame.drop(agenteFrame.columns[[0]], axis=1)

X_test = agenteFrame[agenteFrame.columns[:21]]
y_test = agenteFrame[agenteFrame.columns[21:]]

X_pe, X_test, y_pe, y_test = train_test_split(X_test, y_test, test_size=0.3) # Reducimos mucho el dataset a predecir

######################## MODELO ########################
sherMor = pd.read_csv('../OutFiles/modelo/trainFrame.csv')
sherMor = sherMor.drop(sherMor.columns[[0]], axis=1)

X_train = sherMor[sherMor.columns[:21]]
y_train = sherMor[sherMor.columns[21:]]
clf_rf = RandomForestClassifier(n_estimators = 100, criterion = 'gini', 
                                max_depth=9, max_features = 3, 
                                bootstrap=False, n_jobs=-1, 
                                class_weight="balanced")

clf_rf.fit(X_train, y_train) # Construccion del modelo

########################################################

pickle.dump(clf_rf,open('model.pkl','wb'))

########################################################

preds_rf = clf_rf.predict(X_test) # Test del modelo
features = sherMor.columns[:21]
print("Random Forest: \n" 
      +classification_report(y_true=y_test['26'], y_pred=preds_rf))

# Matriz de confusión
print("Matriz de confusión:\n")
matriz = pd.crosstab(y_test['26'], preds_rf, rownames=['actual'], colnames=['preds'])
print(matriz)

# Variables relevantes

print("Relevancia de variables:\n")
print(pd.DataFrame({'Indicador': features ,
              'Relevancia': clf_rf.feature_importances_}),"\n")
print("Maxima relevancia RF :" , max(clf_rf.feature_importances_), "\n")
